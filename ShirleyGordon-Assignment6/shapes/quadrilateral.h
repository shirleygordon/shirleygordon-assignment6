#ifndef quadrilateral_H
#define quadrilateral_H

#include "quadrilateral.h"
#include "shapeException.h"
#include "shape.h"
#include "circle.h"
#include <iostream>
#include "inputException.h"
#include <string>

class quadrilateral : public Shape {
public:
	void draw();
	double CalArea();
	//quadrilateral();
	quadrilateral(std::string, std::string, int, int, vector<string>& namesVector);
	double CalPerimater();
	double getCalPerimater();
	void setHeight(int h);
	void setWidth(int w);
	int getHeight();
	int getWidth();

private:
	int width;
	int height;
};
#endif