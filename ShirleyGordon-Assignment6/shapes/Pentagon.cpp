#include "Pentagon.h"

/*
C'tor for pentagon object.
Input: name, color, side.
Output: none.
*/
Pentagon::Pentagon(std::string name, std::string color, double side, vector<string>& namesVector) : Shape(name, color, namesVector)
{
	bool tryAgain = false;

	do
	{
		try
		{
			tryAgain = false;
			this->setSide(side);
		}
		catch (std::exception & e)
		{
			tryAgain = true;
			std::cout << e.what() << std::endl << "Please enter valid side length:" << std::endl;
			inputException::getNumericInput(side);
		}
	} while (tryAgain);
}

/*
Setter function for pentagon side.
Input: side.
Output: none.
*/
void Pentagon::setSide(double side)
{
	if (side <= 0)  // Side can't be negative or 0.
	{
		throw shapeException();
	}
	this->_side = side;
}

/*
Function prints a pentagon's details.
Input: none.
Output: none.
*/
void Pentagon::draw()
{
	std::cout << this->getName() << std::endl;
	std::cout << this->getColor() << std::endl;
	std::cout << "Side is " << this->_side << std::endl;
	std::cout << "Area is " << this->CalArea() << std::endl;
	std::cout << "Perimeter is " << this->getPerimeter() << std::endl << std::endl;
}

/*
Function calculates the area of a pentagon and returns it.
Input: none.
Output: area of the pentagon.
*/
double Pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(this->_side);
}

/*
Function calculates the perimeter of a pentagon and returns it.
Input: none.
Output: perimeter of the pentagon.
*/
double Pentagon::getPerimeter()
{
	return PENTAGON_SIDES * this->_side;
}
