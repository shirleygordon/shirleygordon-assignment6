#include "MathUtils.h"

/*
Function calculates the area of a regular pentagon.
Input: a side of the pentagon.
Output: the area of the pentagon.
*/
double MathUtils::CalPentagonArea(double side)
{
	return PENTAGON_AREA * pow(side, SQUARED);
}

/*
Function calculates the area of a regular hexagon.
Input: a side of the hexagon.
Output: the area of the hexagon.
*/
double MathUtils::CalHexagonArea(double side)
{
	return HEXAGON_AREA * pow(side, SQUARED);
}
