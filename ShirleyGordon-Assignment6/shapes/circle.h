#ifndef CIRCLE_H
#define CIRCLE_H

#include <iostream>

#define PI 3.14

class Circle : public Shape
{
private:
	double radius;

public:
	void draw();
	double CalArea();
	double CalCircumference();
	Circle(std::string, std::string, double, vector<string>& namesVector);
	void setRad(double rad);
	double getRad();
};

#endif