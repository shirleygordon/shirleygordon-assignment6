#pragma once
#include <cmath>

#define SQUARED 2
#define PENTAGON_AREA 1.72048
#define HEXAGON_AREA 2.59808

class MathUtils
{
public:
	static double CalPentagonArea(double side);
	static double CalHexagonArea(double side);
};