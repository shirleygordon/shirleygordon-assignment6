#ifndef Shape_H
#define Shape_H
#include <string>
#include <iostream>
#include "inputException.h"
#include <vector>

using std::vector;
using std::string;

class Shape
{
public:
	virtual void draw() =0; //DEFINE FOR ALL
	virtual double CalArea();//DEFINE FOR ALL
	static int getCount();
	void setName(std::string nam);
	std::string getName();
	void setColor(std::string);
	std::string getColor();
	Shape(std::string nam, std::string col, vector<string>& namesVector);
	//Shape();
	~Shape();
private:
	static int count;
	std::string name;
	std::string color;
	//double area;
};

#endif