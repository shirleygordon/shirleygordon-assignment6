#include "inputException.h"

/*
Function checks if the input stream is in fail state and throws an exception if it is.
Input: none.
Output: none.
*/
void inputException::checkFailState()
{
	if (std::cin.fail())
	{
		throw inputException();
	}
}

/*
Function gets rid of failure state of input stream.
Input: none.
Output: none.
*/
void inputException::clearIstream()
{
	std::cin.clear(); // Get rid of failure state
	std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // Discard 'bad' character(s) 
}

/*
Function gets double input.
Input: reference to double variable.
Output: none.
*/
void inputException::getNumericInput(double& input)
{
	bool getInputAgain = false;

	do // Get input
	{
		getInputAgain = false;
		try
		{
			std::cin >> input;
			checkFailState(); // Check if the input is valid. If not, throw exception,.
		}
		catch (std::exception& e)
		{
			getInputAgain = true;
			std::cout << e.what() << std::endl; // Print exception
			clearIstream(); // Clear input stream and try again
		}
	} while (getInputAgain);
}

/*
Function gets integer input.
Input: reference to integer variable.
Output: none.
*/
void inputException::getNumericInput(int& input)
{
	double doubleInput = 0;

	getNumericInput(doubleInput); // Put input into a double variable.
	input = (int)doubleInput; // Cast to int.
}

/*
Function gets shape choice input from user and checks if it's valid.
Input: none.
Output: user's choice.
*/
char choiceInputException::getChoiceInput()
{
	char shapeType = 0;
	std::string buffer;

	std::cin >> buffer; // Put input inside a string buffer

	// Check if the input is longer than 1 character
	if (buffer.length() > 1)
	{
		throw choiceInputException(); // If it is, throw exception
	}

	// If the input is valid, return the character
	shapeType = buffer[0];

	return shapeType;
}

/*
Function checks if the name entered has already been entered before,
and throws an exception if it has been.
Input: names vector reference and new name reference.
Output: none.
*/
void nameInputException::searchName(const vector<string>& names, const string& newName)
{
	unsigned int i = 0;

	// Compare new name to every past name
	for (i = 0; i < names.size(); i++)
	{
		if (names[i] == newName) // If a match is found, throw exception
		{
			throw nameInputException();
		}
	}
}

/*
Function gets a name for the new shape from the user.
Input: names vector reference, name string reference.
Output: none.
*/
void nameInputException::getName(vector<string>& namesVector, string& name)
{
	bool getInputAgain = false;

	do
	{
		try // Get name
		{
			getInputAgain = false;
			std::cin >> name;
			searchName(namesVector, name); // Check if the name has already been entered
		}
		catch (std::exception & e) // If name is invalid (has been entered already), get name again
		{
			getInputAgain = true;
			std::cout << e.what() << std::endl;
		}
	} while (getInputAgain);

	namesVector.push_back(name); // Push the valid name into the names vector
}

/*
Function checks if the color entered by the user is a valid color.
Input: color string reference.
Output: none.
*/
void colorInputException::validateColor(const string& color)
{
	unsigned int i = 0, j = 0;
	bool next = true, matchFound = false;
	const int NUM_OF_COLORS = 12;
	const string COLORS[NUM_OF_COLORS] = {"White", "Black", "Red", "Green", "Blue", "Pink", "Purple", "Brown", "Yellow", "Gray", "Grey", "Orange"};
 
	// Compare new color to every valid color
	for (i = 0; i < NUM_OF_COLORS; i++)
	{
		if (COLORS[i].length() == color.length()) // If the lengths aren't equal, move on to next color
		{
			next = true;
			// Compare each character in the color string
			for (j = 0; j < color.length() && next; j++)
			{
				if (tolower(COLORS[i][j]) != tolower(color[j])) // If the characters don't match, move on to next color
				{
					next = false;
				}
				else if (j == color.length() - 1) // If a match is found, set matchFound to true
				{
					matchFound = true;
				}
			}
		}
	}

	if (!matchFound) // If a match wasn't found, throw exception
	{
		throw colorInputException();
	}
}

/*
Function gets a color for the new shape from the user.
Input: color string reference.
Output: none.
*/
void colorInputException::getColor(string& color)
{
	bool getInputAgain = false;

	do
	{
		try // Get color
		{
			getInputAgain = false;
			std::cin >> color;
			validateColor(color); // Check if the color is a valid color
		}
		catch (std::exception & e) // If color is invalid, get color again
		{
			getInputAgain = true;
			std::cout << e.what() << std::endl;
		}
	} while (getInputAgain);
}

