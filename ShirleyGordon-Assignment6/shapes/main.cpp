#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "inputException.h"
#include "Pentagon.h"
#include "Hexagon.h"
#include <vector>

using std::vector;
using std::string;

int main()
{
	double radius = 1, angle = 0, angle2 = 180;
	int height = 1, width = 1;
	const char CIRCLE = 'c', QUADRILATERAL = 'q', RECTANGLE = 'r', PARALLELOGRAM = 'p', PENTAGON = 't', HEXAGON = 'h';
	char shapeType = 0, addMore = 0;
	bool getInputAgain = false;
	std::string name = "", color = "red"; 
	vector<string> namesVector;

	Circle circ(name, color, radius, namesVector);
	quadrilateral quad(name, color, width, height, namesVector);
	rectangle rec(name, color, width, height, namesVector);
	parallelogram para(name, color, width, height, angle, angle2, namesVector);
	Pentagon pentagon(name, color, width, namesVector);
	Hexagon hexagon(name, color, width, namesVector);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;

	std::cout << "Enter information for your objects" << std::endl << std::endl;
	
	while (addMore != 'x')
	{
		std::cout << "Which shape would you like to create?" << std::endl << "c = circle, q = quadrilateral, r = rectangle, p = parallelogram, t = pentagon, h = hexagon" << std::endl;
		
		// Get choice input from user
		do
		{
			try
			{
				getInputAgain = false;
				shapeType = choiceInputException::getChoiceInput();
			}
			catch (std::exception & e) // If the user entered more than 1 character, the input is invalid.
			{
				getInputAgain = true;
				std::cout << e.what() << std::endl;
			}
		} while (getInputAgain);		
		
		try
		{
			switch (shapeType)
			{
			case CIRCLE:

				std::cout << "Enter color, name and radius for circle." << std::endl;
				
				colorInputException::getColor(color); // Get color
				nameInputException::getName(namesVector, name); // Get name
				inputException::getNumericInput(radius); // Get radius input
								
				circ.setColor(color);
				circ.setName(name);

				try
				{
					circ.setRad(radius);
					ptrcirc->draw();
				}
				catch (std::exception& e)
				{
					std::cout << e.what() << std::endl;
					namesVector.pop_back(); // Remove name from names vector, as the shape wasn't created
				}
				
				break;

			case QUADRILATERAL:

				std::cout << "Enter color, name, height, width." << std::endl;
				
				colorInputException::getColor(color); // Get color
				nameInputException::getName(namesVector, name); // Get name

				// Get height and width input
				inputException::getNumericInput(height);
				inputException::getNumericInput(width);

				quad.setName(name);
				quad.setColor(color);

				try 
				{
					quad.setHeight(height);
					quad.setWidth(width);
					ptrquad->draw();
				}
				catch (std::exception & e) // An exception will be thrown if height or width are negative or 0.
				{
					std::cout << e.what() << std::endl;
					namesVector.pop_back(); // Remove name from names vector, as the shape wasn't created
				}
				
				break;

			case RECTANGLE:

				std::cout << "Enter color, name, height, width." << std::endl;
				
				colorInputException::getColor(color); // Get color				
				nameInputException::getName(namesVector, name); // Get name

				// Get height and width input
				inputException::getNumericInput(height);
				inputException::getNumericInput(width);

				rec.setName(name);
				rec.setColor(color);

				try
				{
					rec.setHeight(height);
					rec.setWidth(width);
					ptrrec->draw();
				}
				catch (std::exception & e) // An exception will be thrown if height or width are negative or 0.
				{
					std::cout << e.what() << std::endl;
					namesVector.pop_back(); // Remove name from names vector, as the shape wasn't created
				}

				break;

			case PARALLELOGRAM:

				std::cout << "Enter color, name, height, width, and 2 angles." << std::endl;
				
				colorInputException::getColor(color); // Get color					
				nameInputException::getName(namesVector, name); // Get name

				// Get height, width and 2 angles input.
				inputException::getNumericInput(height);
				inputException::getNumericInput(width);
				inputException::getNumericInput(angle);
				inputException::getNumericInput(angle2);

				para.setName(name);
				para.setColor(color);

				try
				{
					para.setHeight(height);
					para.setWidth(width);
					para.setAngle(angle, angle2);
					ptrpara->draw();
				}
				catch (std::exception & e) // An exception will be thrown if height or width are negative or 0, or if angles aren't between 0 and 180.
				{
					std::cout << e.what() << std::endl;
					namesVector.pop_back(); // Remove name from names vector, as the shape wasn't created
				}

				break;

			case PENTAGON:

				std::cout << "Enter color, name, and side length." << std::endl;
				
				colorInputException::getColor(color); // Get color					
				nameInputException::getName(namesVector, name); // Get name				
				inputException::getNumericInput(width); // Get side

				pentagon.setName(name);
				pentagon.setColor(color);

				// Set side and print details
				try
				{
					pentagon.setSide(width);
					pentagon.draw();
				}
				catch (std::exception & e) // An exception will be thrown if side is negative or 0.
				{
					std::cout << e.what() << std::endl;
					namesVector.pop_back(); // Remove name from names vector, as the shape wasn't created
				}

				break;

			case HEXAGON:

				std::cout << "Enter color, name, and side length." << std::endl;
				
				colorInputException::getColor(color); // Get color					
				nameInputException::getName(namesVector, name); // Get name
				inputException::getNumericInput(width); // Get side

				hexagon.setName(name);
				hexagon.setColor(color);

				// Set side and print details
				try
				{
					hexagon.setSide(width);
					hexagon.draw();
				}
				catch (std::exception & e) // An exception will be thrown if side is negative or 0.
				{
					std::cout << e.what() << std::endl;
					namesVector.pop_back(); // Remove name from names vector, as the shape wasn't created
				}

				break;

			default:

				std::cout << "You have entered an invalid letter, please re-enter." << std::endl;
				break;
			}
			std::cout << "Would you like to add more objects? press any key. If not, press x." << std::endl;

			// Get choice input from user
			do
			{
				try
				{
					getInputAgain = false;
					addMore = choiceInputException::getChoiceInput();
				}
				catch (std::exception & e) // If the user entered more than 1 character, the input is invalid.
				{
					getInputAgain = true;
					std::cout << "Warning! Don't enter more than one character." << std::endl;
				}
			} while (getInputAgain);
		}
		catch (std::exception& e)
		{			
			std::cout << e.what() << std::endl;
		}
		catch (...)
		{
			std::cout << "Caught a bad exception. Continuing as usual." << std::endl;
		}
	}

	system("pause");
	return 0;
}