#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"
#include "inputException.h"

#define PENTAGON_SIDES 5

class Pentagon : public Shape
{
private:
	double _side;

public:
	Pentagon(std::string name, std::string color, double side, vector<string>& namesVector);
	void setSide(double side);
	void draw();
	double CalArea();
	double getPerimeter();
};