#include "parallelogram.h"
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include <iostream>

parallelogram::parallelogram(std::string col, std::string nam, int h, int w, double ang, double ang2, vector<string>& namesVector) : quadrilateral(col, nam, h, w, namesVector)
{
	bool tryAgain = false;

	do
	{
		try
		{
			tryAgain = false;
			setAngle(ang, ang2);
		}
		catch (std::exception & e)
		{
			tryAgain = true;
			std::cout << e.what() << std::endl << "Please enter two valid angles:" << std::endl;
			inputException::getNumericInput(ang);
			inputException::getNumericInput(ang2);
		}
	} while (tryAgain);
}
void parallelogram::draw()
{
	std::cout <<getName()<< std::endl << getColor() << std::endl << "Height is " << getHeight() << std::endl<< "Width is " << getWidth() << std::endl
		<< "Angles are: " << getAngle()<<","<<getAngle2()<< std::endl <<"Area is "<<CalArea(getWidth(),getHeight())<< std::endl;
}

double parallelogram::CalArea(double w, double h)
{
	if (w < 0 || h < 0)
	{
		throw shapeException();
	}
	return w*h;
}
void parallelogram::setAngle(double ang, double ang2)
{
	// Angles sum must equal 180, and they can't be negative.
	if (ang + ang2 != ANGLES_SUM || ang < 0 || ang2 < 0)
	{
		throw shapeException();
	}
	angle = ang;
	angle2 = ang2;
}

double parallelogram::getAngle()
{
	return angle;
}
double parallelogram::getAngle2()
{
	return angle2;
}
