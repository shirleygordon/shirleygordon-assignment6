#include "shape.h"
#include "circle.h"
#include "shapeException.h"
#include "inputException.h"
#include <iostream>

Circle::Circle(std::string nam, std::string col, double rad, vector<string>& namesVector) : Shape(nam, col, namesVector)
{
	bool tryAgain = false;

	do
	{
		try
		{
			tryAgain = false;
			setRad(rad);
		}
		catch (std::exception & e)
		{
			tryAgain = true;
			std::cout << e.what() << std::endl << "Please enter valid radius:" << std::endl;
			inputException::getNumericInput(rad);
		}
	} while (tryAgain);
}

void Circle::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl;
	std::cout << "Name is " << getName() << std::endl;
	std::cout << "Radius is " << getRad() << std::endl;
	std::cout << "Circumference: " << CalCircumference() << std::endl;
}

void Circle::setRad(double rad)
{
	// Throw exception if the radius is negative or equal to 0.
	if (rad <= 0)
	{
		throw shapeException();
	}

	radius = rad;
}

double Circle::CalArea()
{
	double area = PI * radius * radius;
	return area;
}

double Circle:: getRad()
{
	return radius;
}

double Circle::CalCircumference()
{
	return 2 * PI * radius;
}