#pragma once
#include "shape.h"
#include "MathUtils.h"
#include "shapeexception.h"
#include "inputException.h"

#define HEXAGON_SIDES 6

class Hexagon : public Shape
{
private:
	double _side;

public:

	Hexagon(std::string name, std::string color, double side, vector<string>& namesVector);
	void setSide(double side);
	void draw();
	double CalArea();
	double getPerimeter();
};