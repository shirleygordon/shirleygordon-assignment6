#include "quadrilateral.h"

//quadrilateral::quadrilateral() {}
quadrilateral::quadrilateral(std::string nam, std::string col, int h, int w, vector<string>& namesVector):Shape(nam, col, namesVector)
{
	bool tryAgain = false;

	do // Set height
	{
		try
		{
			tryAgain = false;
			this->setHeight(h);
		}
		catch (std::exception& e) // If height is negative
		{
			tryAgain = true;
			std::cout << e.what() << std::endl << "Please enter valid height:" << std::endl;
			inputException::getNumericInput(h);
		}
	} while (tryAgain);

	do // Set width
	{
		try
		{
			tryAgain = false;
			this->setWidth(w);
		}
		catch (std::exception & e) // If width is negative
		{
			tryAgain = true;
			std::cout << e.what() << std::endl << "Please enter valid width:" << std::endl;
			inputException::getNumericInput(w);
		}
	} while (tryAgain);
}

void quadrilateral::draw()
{
	//quadrilateral p;
	std::cout << getName()<< std::endl << getColor() << std::endl<< "Width is " << getWidth() << std::endl << "Height is " << getHeight() << std::endl <<"Area is "<<CalArea()<< std::endl<<"Perimeter is "<<getCalPerimater()<< std::endl;
	//cout << name << color;
}

double quadrilateral::CalArea()
{
	if (width < 0 || height < 0)
	{
		throw shapeException();
	}
	return width * height; //RECTANGLE 
}

void quadrilateral::setHeight(int h)
{
	if (h <= 0) // Height can't be 0 or negative.
	{
		throw shapeException();
	}
	height = h;
}

void quadrilateral::setWidth(int w)
{
	if (w <= 0) // Width can't be 0 or negative.
	{
		throw shapeException();
	}
	width = w;
}

double quadrilateral::CalPerimater()
{
	return 2 * (height + width);
}

double quadrilateral::getCalPerimater()
{
	return 2 * (height + width);
}

int quadrilateral::getHeight()
{
	return height;
}

int quadrilateral::getWidth()
{
	return width;
}