#include "shape.h"
//#include "circle.h"
#include <iostream>

Shape::Shape(std::string nam, std::string col, vector<string>& namesVector)
{
	try // Check if name is valid
	{
		nameInputException::searchName(namesVector, nam);
	}
	catch (std::exception & e) // If not, get name again
	{
		std::cout << e.what() << std::endl;
		nameInputException::getName(namesVector, nam);
	}

	this->setName(nam); // Finally, set name
	this->setColor(col); // Set color 
	count++;
}

 int Shape::count = 0;
 //Shape::Shape(){}
int Shape::getCount() {
	{
		return count;
	}
}
double Shape::CalArea() {
	std::cout << "Unknown area" << std::endl;
	return 0;
}
Shape::~Shape()
{
	count--;

	//cout << name << endl << color << endl << "Area is " << CalArea() << endl;; 
}

void Shape::setName(std::string nam)
{
	name = nam;
}

std::string Shape::getName()
{
	return name;
}

void Shape::setColor(std::string col)
{
	try // Check if the color is valid
	{
		colorInputException::validateColor(col);
	}
	catch (std::exception & e) // If not, get color again
	{
		std::cout << e.what() << std::endl;
		colorInputException::getColor(col);
	}
	
	color = col; // Finally, set color
}
std::string Shape::getColor() {
	return color;
}