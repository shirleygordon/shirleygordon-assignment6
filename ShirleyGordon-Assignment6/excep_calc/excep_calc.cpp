#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::endl;
using std::cerr;

#define INVALID_NUM 8200
#define TEST_NUM 5

/*
Function checks if a is 8200 and throws an exception if it is.
Input: a number.
Output: none.
*/
void excep8200(const int a)
{
	if (a == INVALID_NUM)
	{
		throw(string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."));
	}
}

int add(int a, int b)
{
	// Throw exception if a or b are 8200
	excep8200(a); 
	excep8200(b);
	excep8200(a + b);

	return a + b;
}

int multiply(int a, int b) 
{
	int sum = 0;
	
	// Throw exception if a or b are 8200
	excep8200(a);
	excep8200(b);

	for(int i = 0; i < b; i++)
	{
		excep8200(i); // Throw exception if i is 8200
		
		sum = add(sum, a);

		// Throw exception if sum is 8200
		excep8200(sum);
	}
	return sum;
}

int pow(int a, int b)
{
	int exponent = 1;

	// Throw exception if a or b are 8200
	excep8200(a); 
	excep8200(b);

	for(int i = 0; i < b; i++)
	{
		excep8200(i); // Throw exception if i is 8200
		
		exponent = multiply(exponent, a);

		excep8200(exponent); // Throw exception if exponent is 8200
	}

	return exponent;
}

int main(void)
{
	try
	{
		cout << "Result of adding two valid numbers: " << add(TEST_NUM, TEST_NUM) << endl << endl;
	}
	catch (const string& e)
	{
		cerr << "ERROR: " << e << endl << endl;
	}

	try
	{
		cout << "Result of adding 8200: " << add(INVALID_NUM, TEST_NUM) << endl << endl;
	}
	catch (const string & e)
	{
		cerr << "ERROR: " << e << endl << endl;
	}

	try
	{
		cout << "Result of adding two numbers whose result is 8200: " << add(INVALID_NUM - 1, 1) << endl << endl;
	}
	catch (const string & e)
	{
		cerr << "ERROR: " << e << endl << endl;
	}

	try
	{
		cout << "Result of multiplying two valid numbers: " << multiply(TEST_NUM, TEST_NUM) << endl << endl;
	}
	catch (const string & e)
	{
		cerr << "ERROR: " << e << endl << endl;
	}

	try
	{
		cout << "Result of multiplying 8200: " << multiply(INVALID_NUM, TEST_NUM) << endl << endl;
	}
	catch (const string & e)
	{
		cerr << "ERROR: " << e << endl << endl;
	}

	try
	{
		cout << "Result of multiplying two numbers whose result is 8200: " << multiply(INVALID_NUM / TEST_NUM, TEST_NUM) << endl << endl;
	}
	catch (const string & e)
	{
		cerr << "ERROR: " << e << endl << endl;
	}

	try
	{
		cout << "Result of pow of two valid numbers: " << pow(TEST_NUM, TEST_NUM) << endl << endl;
	}
	catch (const string & e)
	{
		cerr << "ERROR: " << e << endl << endl;
	}

	try
	{
		cout << "Result of pow of 8200: " << pow(INVALID_NUM, TEST_NUM) << endl << endl;
	}
	catch (const string & e)
	{
		cerr << "ERROR: " << e << endl << endl;
	}
}