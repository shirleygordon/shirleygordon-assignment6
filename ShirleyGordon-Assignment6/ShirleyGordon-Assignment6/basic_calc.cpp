#include <iostream>

using std::cout;
using std::endl;
using std::cerr;

#define ERROR -1
#define NO_ERROR 0
#define INVALID_NUM 8200
#define TEST_NUM 5

/*
Function adds a to b and puts the result in result.
Input: a, b, result.
Output: -1 if 8200 was used anywhere in the calculation, 0 if not.
*/
int add(int a, int b, int& result)
{
	int error = ERROR;

	// Check if the invalid number is used in any of the variables
	if (a != INVALID_NUM && b != INVALID_NUM && (a + b) != INVALID_NUM)
	{
		// If not, proceed as usual
		result = a + b;
		error = NO_ERROR;
	}

	return error; // Return whether an error occurred or not 
}

/*
Function multiplies a by b and puts the result in sum.
Input: a, b, sum.
Output: -1 if 8200 was used anywhere in the calculation, 0 if not.
*/
int  multiply(int a, int b, int& sum)
{
	int isError = NO_ERROR;

	// Add a to itself b times.
	for(int i = 0; i < b && isError != ERROR; i++)
	{
		isError = add(sum, a, sum);

		// If i is 8200, there's an error. 
		if (i == INVALID_NUM)
		{
			isError = ERROR;
		}
	}

	return isError;
}

/*
Function raises a to the power of b and puts the result in exponent.
Input: a, b, exponent.
Output: -1 if 8200 was used anywhere in the calculation, 0 if not.
*/
int pow(int a, int b, int& exponent)
{
	int isError = NO_ERROR;
	
	// Make sure the variables aren't an invalid num
	if (a == INVALID_NUM || b == INVALID_NUM || exponent == INVALID_NUM)
	{
		isError = ERROR;
	}
	else
	{
		exponent = 1;
		// Multiply the base (a) by itself b times. 
		for (int i = 0; i < b && isError != ERROR; i++)
		{
			isError = multiply(exponent, (a-1), exponent);
			
			// If i is 8200, there's an error
			if (i == INVALID_NUM)
			{
				isError = ERROR;
			}
		}
	}
	
	return isError;
}

int main(void)
{
	int result = 0, num = 0;

	cout << "Try to add 8200:" << endl;
	if (add(INVALID_NUM, TEST_NUM, result) == ERROR)
	{
		cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year." << endl << endl;
	}
	else
	{
		cout << "Result: " << result << endl << endl;
	}
	result = 0;

	cout << "Try to add two valid numbers:" << endl;
	if (add(TEST_NUM, TEST_NUM, result) == ERROR)
	{
		cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year." << endl << endl;
	}
	else
	{
		cout << "Result of adding two valid numbers: " << result << endl << endl;
	}
	result = 0;

	cout << "Try to add two numbers whose sum equals to 8200:" << endl;
	num = INVALID_NUM - 1;
	if (add(num, 1, result) == ERROR)
	{
		cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year." << endl << endl;
	}
	else
	{
		cout << "Result: " << result << endl << endl;
	}
	result = 0;

	cout << "Try to multiply 8200:" << endl;
	if (multiply(INVALID_NUM, TEST_NUM, result) == ERROR)
	{
		cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year." << endl << endl;
	}
	else
	{
		cout << "Result: " << result << endl << endl;
	}
	result = 0;

	cout << "Try to multiply two numbers whose result is 8200:" << endl;
	num = INVALID_NUM / TEST_NUM;
	if (multiply(num, TEST_NUM, result) == ERROR)
	{
		cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year." << endl << endl;
	}
	else
	{
		cout << "Result: " << result << endl << endl;
	}
	result = 0;

	cout << "Try to multiply two valid numbers:" << endl;
	if (multiply(TEST_NUM, TEST_NUM, result) == ERROR)
	{
		cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year." << endl << endl;
	}
	else
	{
		cout << "Result of multiplying two valid numbers: " << result << endl << endl;
	}
	result = 0;

	cout << "Try to pow two valid numbers:" << endl;
	if (pow(TEST_NUM, TEST_NUM, result) == ERROR)
	{
		cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year." << endl << endl;
	}
	else
	{
		cout << "Result of pow of two valid numbers: " << result << endl << endl;
	}
	result = 0;

	cout << "Try to pow 8200:" << endl;
	if (pow(INVALID_NUM, TEST_NUM, result) == ERROR)
	{
		cerr << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year." << endl << endl;
	}
	else
	{
		cout << "Result of pow of two valid numbers: " << result << endl << endl;
	}

	return 0;
}